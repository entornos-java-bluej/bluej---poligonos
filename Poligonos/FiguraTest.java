

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class FiguraTest.
 *
 * @author  (Arturo Sierra y Miguel Ángel López)
 * @version (28/02/2019)
 */
public class FiguraTest
{
    /**
     * Default constructor for test class FiguraTest
     */
    Figura t;
    public FiguraTest()
    {
    }
    
    //Tests for figures of three sides
    @Test 
    public void prueba1() // abajo == derecho == izquierdo
    {
        t = new Figura(3,3,3,0,0,true);
        assertEquals("Equilatero",t.dimeTipo());
    }

    @Test
    public void prueba2() // abajo != derecho == izquierdo && 90º=false
    {
        t = new Figura(5,3,3,0,0,false);
        assertEquals("Isosceles",t.dimeTipo());
    }

    @Test
    public void prueba3() // abajo == derecho != izquierdo && 90º=false
    {
        t = new Figura(3,3,5,0,0,false);
        assertEquals("Isosceles",t.dimeTipo());
    }

    @Test
    public void prueba4() // derecho == abajo != izquierdo && 90º=false
    {
        t = new Figura(3,5,3,0,0,false);
        assertEquals("Isosceles",t.dimeTipo());
    }
    
    @Test
    public void prueba5() // abajo != derecho == izquierdo && 90º=true
    {
        t = new Figura(5,3,3,0,0,true);
        assertEquals("Triangulo rectangulo",t.dimeTipo());
    }

    @Test
    public void prueba6() // abajo == derecho != izquierdo && 90º=true
    {
        t = new Figura(3,3,5,0,0,true);
        assertEquals("Triangulo rectangulo",t.dimeTipo());
    }

    @Test
    public void prueba7() // derecho == abajo != izquierdo && 90º=true
    {
        t = new Figura(3,5,3,0,0,true);
        assertEquals("Triangulo rectangulo",t.dimeTipo());
    }
    
    @Test
    public void prueba8() // derecho != izquierdo != abajo && 90º=false
    {
        t = new Figura(1,2,3,0,0,false);
        assertEquals("Escaleno",t.dimeTipo());
    }

    @Test
    public void prueba9() // derecho =0 o izquierdo =0 or abajo =0 && 90º=false
    {
        t = new Figura(3,0,3,0,0,false);
        assertEquals("Los lados derecho, izquierdo y abajo deben tener un valor positivo entre 0 y 100",t.dimeTipo());
    }

    @Test
    public void prueba10() // izquierdo isn't negative value
    {
        t = new Figura(3,-1,3,0,0,false);
        assertEquals("Los lados derecho, izquierdo y abajo deben tener un valor positivo entre 0 y 100",t.dimeTipo());
    }
    
    @Test
    public void prueba11() // izquierdo isn't not empty
    {
        t = new Figura(3,' ',3,0,0,false);
        assertEquals("Los lados derecho, izquierdo y abajo deben tener un valor positivo entre 0 y 100",t.dimeTipo());
    }
    
    @Test 
    public void prueba12() // derecho=0  izquierdo=0  abajo =0
    {
        t = new Figura(0,0,0,0,0,true);
        assertEquals("Los lados derecho, izquierdo y abajo deben tener un valor positivo entre 0 y 100",t.dimeTipo());
    }
    
    @Test 
    public void prueba13() // derecho>100  izquierdo>100  abajo>100
    {
        t = new Figura(10,25,101,0,0,true);
        assertEquals("Los lados derecho, izquierdo y abajo deben tener un valor positivo entre 0 y 100",t.dimeTipo());
    }
    
    @Test 
    public void prueba14() // upper limit
    {
        t = new Figura(100,27,65,0,0,true);
        assertEquals("Escaleno",t.dimeTipo());
    }
    
    @Test 
    public void prueba15() // lower limit
    {
        t = new Figura(1,1,1,0,0,true);
        assertEquals("Equilatero",t.dimeTipo());
    }
    
    //Tests for figures of three sides
    @Test 
    public void prueba16() // arriba isn't negative value 
    {
        t = new Figura(1,1,1,-1,0,true);
        assertEquals("Los lados arriba y extra deben tener un valor positivo entre 0 y 100",t.dimeTipo());
    }
    
    @Test 
    public void prueba17() // lower limit
    {
        t = new Figura(1,1,1,1,0,true);
        assertEquals("Cuadrado",t.dimeTipo());
    }
    
    @Test 
    public void prueba18() // arriba cant't be greater than 100
    {
        t = new Figura(1,1,1,101,0,true);
        assertEquals("Los lados arriba y extra deben tener un valor positivo entre 0 y 100",t.dimeTipo());
    }
    
    @Test 
    public void prueba19() // upper limit 
    {
        t = new Figura(27,10,10,100,0,true);
        assertEquals("Trapecio",t.dimeTipo());
    }
    
    @Test 
    public void prueba20() // arriba isn't not empty 
    {
        t = new Figura(27,10,10,' ',0,true);
        assertEquals("Los lados arriba y extra deben tener un valor positivo entre 0 y 100",t.dimeTipo());
    }
    
    @Test 
    public void prueba21() // abajo == derecho == izquierdo == arriba && angulo90º=true
    {
        t = new Figura(3,3,3,3,0,true);
        assertEquals("Cuadrado",t.dimeTipo());
    }
   
    @Test 
    public void prueba22() // abajo == derecho == izquierdo == arriba && angulo90º=false
    {
        t = new Figura(3,3,3,3,0,false);
        assertEquals("Rombo",t.dimeTipo());
    }
    
    @Test 
    public void prueba23() // abajo == arriba && derecho == izquierdo angulo90º=true
    {
        t = new Figura(3,3,2,2,0,true);
        assertEquals("Rectangulo",t.dimeTipo());
    }
    
    @Test 
    public void prueba24() // abajo == arriba && derecho == izquierdo angulo90º=false
    {
        t = new Figura(3,3,2,2,0,false);
        assertEquals("Romboide",t.dimeTipo());
    }
    
    @Test 
    public void prueba25() // arriba == abajo && derecho != izquierdo 
    {
        t = new Figura(19,28,33,33,0,true);
        assertEquals("Trapecio",t.dimeTipo());
    }
    
    @Test 
    public void prueba26() // arriba == izquierdo && derecho != abajo 
    {
        t = new Figura(19,33,28,33,0,true);
        assertEquals("Trapecio",t.dimeTipo());
    }
    
    @Test 
    public void prueba27() // arriba != izquierdo && derecho == abajo 
    {
        t = new Figura(88,3,88,14,0,true);
        assertEquals("Trapecio",t.dimeTipo());
    }
    
    @Test 
    public void prueba28() // arriba != abajo && derecho == izquierdo 
    {
        t = new Figura(96,96,7,33,0,true);
        assertEquals("Trapecio",t.dimeTipo());
    }
    
    @Test 
    public void prueba29() // arriba != derecho && izquierdo == abajo 
    {
        t = new Figura(19,28,28,33,0,true);
        assertEquals("Trapecio",t.dimeTipo());
    }
    
    @Test 
    public void prueba30() // arriba == derecho && izquierdo != abajo 
    {
        t = new Figura(19,28,33,19,0,true);
        assertEquals("Trapecio",t.dimeTipo());
    }
    
    @Test 
    public void prueba31() // arriba =! derecho && izquierdo != abajo 
    {
        t = new Figura(19,28,33,20,0,true);
        assertEquals("No es un paralelogramo",t.dimeTipo());
    }
    
    //Test Figuras cinco lados
    @Test 
    public void prueba32() // extra isn't negative value 
    {
        t = new Figura(1,1,1,0,-1,true);
        assertEquals("Los lados arriba y extra deben tener un valor positivo entre 0 y 100",t.dimeTipo());
    }
    
    @Test 
    public void prueba33() // lower limit
    {
        t = new Figura(1,1,1,1,1,true);
        assertEquals("Pentagono regular",t.dimeTipo());
    }
    
    @Test 
    public void prueba34() // extra can't be greater than 100 
    {
        t = new Figura(1,1,1,1,101,true);
        assertEquals("Los lados arriba y extra deben tener un valor positivo entre 0 y 100",t.dimeTipo());
    }
    
    @Test 
    public void prueba35() // upper limit 
    {
        t = new Figura(1,1,1,1,100,true);
        assertEquals("Pentagono irregular",t.dimeTipo());
    }
    
    @Test 
    public void prueba36() // extra isn't not empty 
    {
        t = new Figura(27,10,11,12,' ',true);
        assertEquals("Los lados arriba y extra deben tener un valor positivo entre 0 y 100",t.dimeTipo());
    }
    
    @Test 
    public void prueba37() // abajo == derecho == izquierdo == arriba == extra && angulo90º=true
    {
        t = new Figura(3,3,3,3,3,true);
        assertEquals("Pentagono regular",t.dimeTipo());
    }
    
    @Test 
    public void prueba38() // abajo != derecho != izquierdo != arriba != extra && angulo90º=true
    {
        t = new Figura(2,93,53,37,35,true);
        assertEquals("Pentagono irregular",t.dimeTipo());
    }
    
    
}
