    
    /**
     * This program make a poligon with 3 or 4 or 5 sites and tell you what´s the poligon you have
     * 
     * @author (Arturo Sierra y Miguel Ángel López) 
     * @version (28/02/2019)
     */
    public class Figura
    {
        // Instance variables
        double a,
               b,
               c,
               d,
               e;
        boolean f;
    
        /**
         * Constructor for objects of class Figura
         */
        public Figura(){
            // Initialise instance variables with value 1 in 'a', 'b' and 'c' to create a triangle
            a=1;
            b=1;
            c=1;
            d=0;
            e=0;
            f=true;
        }
        // In this function the user inserts his data
        public Figura(double derecho, double izquierdo, double abajo, double arriba, double extra, boolean angulo90){
            a=derecho;
            b=izquierdo;
            c=abajo;
            d=arriba;
            e=extra;
            f=angulo90;
        }
    
        // Checking data
        public String dimeTipo(){
          if (a<1 || b<1 || c<1 || d<0 || e<0                        // Case of mistakes
             || a==' ' || b==' ' || c==' ' || d==' ' || e==' ' 
             || a>100 || b>100 || c>100 || d>100 || e>100){
                if (a<1 || b<1 || c<1                                // Derecho or izquierdo or abajo can't be less than 1 
                   || a==' ' || b==' ' || c==' '                     // Derecho or izquierdo or abajo can't be NULL
                   || a>100 || b>100 || c>100)                       // Derecho or izquierdo or abajo cant't be greater than 100
                        return ("Los lados derecho, izquierdo y abajo deben tener un valor positivo entre 0 y 100");
                else if ( d<0 || e<0                                 // Arriba or extra can't be less than 0
                        || d==' ' || e==' '                          // Arriba or extra can't be NULL
                        || d>100 || e>100)                           // Arriba or extra can't be greater than 100
                        return ("Los lados arriba y extra deben tener un valor positivo entre 0 y 100");
          return ("Error, introduce numeros");
        }
        else{                                                     // Valid cases of figures of three sides
        if(e==0){                                                   // Extra value 0
            if(d==0){                                                 // Extra and Arriba value 0
                if (b == a && a == c)                                   // All sides are the same value
                return ("Equilatero");
            else{                                                   
                 if (b != a && b != c && c != a)                        // All sides aren't the same value
                     return ("Escaleno");
                 else{                                                  // Two sides are the same value except one
                        if ( f == true)                                    // One angle is 90º
                            return ("Triangulo rectangulo");
                        else                                              // One angle isn't 90º
                            return ("Isosceles");                       
                 }
                 }
            }
            else{                                                // Valid cases of figures of four sides
            if (b == a && b == c                                    
               && b == d && d == c){                              // All sides are the same value
                if (f == true)                                      // All angles are 90º
                    return("Cuadrado");
                else                                                // Not all angles are 90º
                    return("Rombo");
            }
            else{                                                 // Not all sides are the same value
                if (d == c                                        // Arriba is the same value of abajo and izquierdo is the same value of derecho
                   && a == b 
                    ){
                    if (f == true)                                     // All angles are 90º
                        return("Rectangulo");
                    else                                              // All angles aren't 90º 
                        return("Romboide");
                } else {                                            // Two sides are the same value and the other two are different
                        if (d != c && a == b                          // Arriba is different that abajo and derecho is the same value of izquierdo
                           || d == c && a != b                        // Arriba is the same value of abajo and derecho is different that izquierdo
                           || d == b && a != c                        // Arriba is the same value of izquierdo and derecho is different that abajo
                           || d != b && a == c                        // Arriba is different that izquierdo and derecho is the same value of abajo
                           || d != a && b == c                        // Arriba is diferent that derecho and izquierdo is the same value of abajo
                           || d == a && b != c                        // Arriba is the same value of derecho and izquierdo is different that abajo
                            )

                            return("Trapecio");
                        else if (d != c && a != b)                 // Arriba is different value of abajo and derecho than izquierdo
                            return("No es un paralelogramo");
                        }
                    }
                            return ("no es un poligono");
            }
        }
        else{                                                   // Valid cases of figures of five sides
            if (a == b && b == c && c == d && d == e)             // All sides are the same value
        return ("Pentagono regular");
        else                                                      // Not all sides are the same value
        return ("Pentagono irregular");
        }
        }
    }
 }
